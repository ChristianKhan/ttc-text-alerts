import bs4 as bs
import urllib.request
import os
from twilio.rest import Client

#Connect to website
sauce = urllib.request.urlopen('http://www.ttc.ca/mobile/all_service_alerts.jsp').read()

#Parse through website (HTML code) 
soup = bs.BeautifulSoup(sauce,'lxml')


#Search for "Line 1" using lambda function 
for paragraph in soup.find_all(lambda tag: "Line 1" in tag.string if tag.string else False): 

	print(paragraph.getText())

#Create sendMessage function 
def sendMessage(): 

#Authenticate Client
	account_sid = os.environ.get('TWILIO_SID')
	auth_token = os.environ.get('TWILIO_TOKEN')

#client object that is an instance of Client class from twilio api
	client = Client(account_sid, auth_token)

#create new message relay from 2 phone numbers with content from search loop
	client.messages.create(
		to = os.environ.get('MY_NUM'),
		from_ =os.environ.get('TWILIO_NUM'),
		body = paragraph
		)

#call sendMessage() function 
sendMessage()


'''
#COMPLETED!
0.1 Initial setup 		- scrape
				- print results
				- send message to phone 

#COMPLETED!
0.2	Process results 	- Filter results to look for specific text == "501 Queen" || "Line 1" etc

0.3 Logic 	    	         - If results != null 
				send results message
				else: send "Nothing to Report here"

0.4 Task Manager 		- Schedule task to run  scraper script on MacOS at specific times
							 Either : CRON JOBS
							 	Scripting
							 	Launchd 

1.0				- Expected Results : 
                                            Daily Alerts for "501 QUEEN" Streetcar and "Line 1"
						    	 sent through text messaging 
'''


